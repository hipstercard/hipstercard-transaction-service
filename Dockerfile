FROM openjdk:17-jdk-alpine
WORKDIR /hipstercard/hipstercard-transaction-service
COPY target/hipstercard-transaction-service.jar hipstercard-transaction-service.jar
RUN pwd
RUN ls
EXPOSE 8083
ENTRYPOINT ["java", "-jar", "./hipstercard-transaction-service.jar"]
