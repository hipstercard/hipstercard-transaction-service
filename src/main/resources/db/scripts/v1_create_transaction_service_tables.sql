CREATE TABLE IF NOT EXISTS organization
(
    code                VARCHAR(255) PRIMARY KEY,
    name                VARCHAR(255)   NOT NULL UNIQUE,
    cashback_percent    DECIMAL(10, 2) NOT NULL,
    contribution_status VARCHAR(255)   NOT NULL
);

CREATE TABLE IF NOT EXISTS operation_category
(
    name                VARCHAR(255) PRIMARY KEY,
    cashback_percent    DECIMAL(10, 2) NOT NULL,
    contribution_status VARCHAR(255)   NOT NULL
);

CREATE TABLE IF NOT EXISTS organization_flexible_cashback
(
    id                  VARCHAR(255) PRIMARY KEY,
    organization_code   VARCHAR(255)   NOT NULL,
    min_transaction_sum DECIMAL(10, 2) NOT NULL,
    cashback_percent    DECIMAL(10, 2) NOT NULL,
    contribution_status VARCHAR(255)   NOT NULL,
    FOREIGN KEY (organization_code) REFERENCES organization (code)
);

CREATE TABLE IF NOT EXISTS transaction
(
    id                         VARCHAR(255) PRIMARY KEY,
    bank_account_number        VARCHAR(255)             NOT NULL,
    card_id                    VARCHAR(255)             NOT NULL,
    date                       TIMESTAMP WITH TIME ZONE NOT NULL,
    transaction_sum            DECIMAL(10, 2)           NOT NULL,
    balance_before_transaction DECIMAL(10, 2)           NOT NULL,
    organization_code          VARCHAR(255)             NOT NULL,
    operation_category_name    VARCHAR(255)             NOT NULL,
    FOREIGN KEY (organization_code) REFERENCES organization (code),
    FOREIGN KEY (operation_category_name) REFERENCES operation_category (name)
);
