INSERT INTO organization (code, name, cashback_percent, contribution_status)
VALUES ('organization:e44bde32-b7d2-470d-849d-72dd040d70ae', 'Yandex-Market', 1, 'APPROVED'),
       ('organization:9aac21a0-b3dc-4836-8aa4-537193745c10', 'Ozon', 1.5, 'APPROVED'),
       ('organization:aa4ac745-31d0-4bdf-bc25-c1fc30fa3371', 'Magnit', 3, 'APPROVED'),
       ('organization:qbe7eeb9-6fc4-497b-bb9b-4d7e1c199df4', 'Lenta', 4.5, 'APPROVED'),
       ('organization:39a97e1d-d8a7-42e8-850b-f663ad8c0e33', 'Steam', 5, 'APPROVED'),
       ('organization:1d267f37-1183-4735-919d-e243fd9a6c37', 'Nike', 50, 'DECLINED');

INSERT INTO operation_category (name, cashback_percent, contribution_status)
VALUES ('TRAVEL', 1, 'APPROVED'),
       ('RESTAURANTS', 3.5, 'APPROVED'),
       ('VIDEOGAMES', 2, 'APPROVED'),
       ('BOOKS', 5.5, 'APPROVED'),
       ('GAS_STATIONS', 2, 'APPROVED'),
       ('MUSIC', 50, 'DECLINED');

INSERT INTO organization_flexible_cashback (id, organization_code, min_transaction_sum, cashback_percent,
                                            contribution_status)
VALUES ('organization-flexible-cashback:8cdf9254-f67d-4881-9279-8a0e4f411c0a',
        'organization:9aac21a0-b3dc-4836-8aa4-537193745c10', 10000, 2, 'APPROVED'),   -- Ozon
       ('organization-flexible-cashback:54340de2-eae2-4731-b1fb-90026688cf40',
        'organization:9aac21a0-b3dc-4836-8aa4-537193745c10', 50000, 4.5, 'APPROVED'), -- Ozon
       ('organization-flexible-cashback:bd44cc93-cd36-48a5-af1b-162f76849b1d',
        'organization:9aac21a0-b3dc-4836-8aa4-537193745c10', 150000, 5, 'APPROVED'),  -- Ozon
       ('organization-flexible-cashback:ad6ed72f-632b-4962-9147-815e288eb4ac',
        'organization:9aac21a0-b3dc-4836-8aa4-537193745c10', 1500000, 30, 'DECLINED'); -- Ozon


INSERT INTO transaction (id, bank_account_number, card_id, date, transaction_sum, balance_before_transaction,
                         organization_code, operation_category_name)
VALUES ('transaction:c6146027-58df-4c5b-a8a8-fb9bd78fae91',
        'bank-account:6cc81406-ac5d-483a-b757-1755da3a309c',
        'card:b6e579d0-6765-4c5e-be1f-2fe60276e1ab',
        '2023-08-04 13:45:18+00:00', 500, 100000,
        'organization:qbe7eeb9-6fc4-497b-bb9b-4d7e1c199df4',
        'VIDEOGAMES');
-- cards: {HIPSTER_PLATINUM (2%)}, Lenta (4.5%), Video games (2%)
