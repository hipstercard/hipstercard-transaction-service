package hipstercard.dev.transactionservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class HipstercardTransactionsServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(HipstercardTransactionsServiceApplication.class, args);
    }
}
