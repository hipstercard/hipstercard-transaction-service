package hipstercard.dev.transactionservice.repository;

import hipstercard.dev.transactionservice.domain.OperationCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface OperationCategoryRepository extends JpaRepository<OperationCategory, String> {
    Optional<OperationCategory> findByName(String name);
}
