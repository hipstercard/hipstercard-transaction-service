package hipstercard.dev.transactionservice.repository;

import hipstercard.dev.transactionservice.domain.OrganizationFlexibleCashback;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrganizationFlexibleCashbackRepository
        extends JpaRepository<OrganizationFlexibleCashback, String> {
    List<OrganizationFlexibleCashback> findAllByOrganizationCode(String organizationCode);
}
