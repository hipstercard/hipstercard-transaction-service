package hipstercard.dev.transactionservice.clients;

import hipstercard.dev.transactionservice.dto.BankAccountCardsDTO;
import hipstercard.dev.transactionservice.dto.BankAccountDTO;
import jakarta.validation.Valid;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "cards-client", url = "http://hipstercard-card-service:8082/api/v1/cards")
public interface CardsClient {
    @PostMapping("/get-linked-cards")
    BankAccountCardsDTO getLinkedCards(@Valid @RequestBody BankAccountDTO bankAccountDTO);
}
