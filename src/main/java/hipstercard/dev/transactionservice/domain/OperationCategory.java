package hipstercard.dev.transactionservice.domain;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;

import java.math.BigDecimal;

@Entity
@Table(name = "operation_category")
@Getter
public class OperationCategory {
    @Id
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "cashback_percent", nullable = false)
    private BigDecimal cashbackPercent;

    @Enumerated(EnumType.STRING)
    @Column(name = "contribution_status", nullable = false)
    private ContributionStatus contributionStatus;
}
