package hipstercard.dev.transactionservice.domain;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.Getter;

import java.math.BigDecimal;

@Entity
@Table(name = "organization_flexible_cashback")
@Getter
public class OrganizationFlexibleCashback {
    @Id
    private String id;

    @OneToOne
    @JoinColumn(name = "organization_code", referencedColumnName = "code", nullable = false)
    private Organization organization;

    @Column(name = "min_transaction_sum", nullable = false)
    private BigDecimal minTransactionSum;

    @Column(name = "cashback_percent", nullable = false)
    private BigDecimal cashbackPercent;

    @Enumerated(EnumType.STRING)
    @Column(name = "contribution_status", nullable = false)
    private ContributionStatus contributionStatus;
}
