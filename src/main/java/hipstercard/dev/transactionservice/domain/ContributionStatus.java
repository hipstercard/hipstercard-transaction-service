package hipstercard.dev.transactionservice.domain;

public enum ContributionStatus {
    CREATED,
    APPROVED,
    DECLINED,
    DELETED,
}
