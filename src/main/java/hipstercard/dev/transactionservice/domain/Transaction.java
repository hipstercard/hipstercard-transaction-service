package hipstercard.dev.transactionservice.domain;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "transaction")
public class Transaction {
    @Id
    private String id;

    @Column(name = "bank_account_number", nullable = false)
    private String bankAccountNumber;

    @Column(name = "card_id", nullable = false)
    private String cardId;

    @Column(name = "date", nullable = false)
    private ZonedDateTime date;

    @Column(name = "transaction_sum", nullable = false)
    private BigDecimal transactionSum;

    @Column(name = "balance_before_transaction", nullable = false)
    private BigDecimal balanceBeforeTransaction;

    @OneToOne
    @JoinColumn(name = "organization_code", referencedColumnName = "code", nullable = false)
    private Organization organization;

    @OneToOne
    @JoinColumn(name = "operation_category_name", referencedColumnName = "name", nullable = false)
    private OperationCategory operationCategory;
}
