package hipstercard.dev.transactionservice.config;

import lombok.Getter;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Getter
public class CashbackTransactionProducerConfiguration {
    @Value("${spring.rabbitmq.exchanges.cashback-transaction-internal}")
    private String internalExchange;

    @Value("${spring.rabbitmq.queues.cashback-transaction}")
    private String cashbackTransactionQueue;

    @Value("${spring.rabbitmq.routing-keys.internal-cashback-transaction}")
    private String internalCashbackTransactionRoutingKey;

    @Bean
    public TopicExchange cashbackTransactionInternalTopicExchange() {
        return new TopicExchange(internalExchange);
    }

    @Bean
    public Queue cashbackTransactionQueue() {
        return new Queue(cashbackTransactionQueue);
    }

    @Bean
    public Binding internalToCashbackTransactionBinding() {
        return BindingBuilder
                .bind(cashbackTransactionQueue())
                .to(cashbackTransactionInternalTopicExchange())
                .with(internalCashbackTransactionRoutingKey);
    }
}
