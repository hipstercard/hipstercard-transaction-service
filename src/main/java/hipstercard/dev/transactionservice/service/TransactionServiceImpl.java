package hipstercard.dev.transactionservice.service;

import hipstercard.dev.transactionservice.clients.CardsClient;
import hipstercard.dev.transactionservice.domain.ContributionStatus;
import hipstercard.dev.transactionservice.domain.OperationCategory;
import hipstercard.dev.transactionservice.domain.Organization;
import hipstercard.dev.transactionservice.domain.OrganizationFlexibleCashback;
import hipstercard.dev.transactionservice.domain.Transaction;
import hipstercard.dev.transactionservice.dto.BankAccountCardsDTO;
import hipstercard.dev.transactionservice.dto.BankAccountDTO;
import hipstercard.dev.transactionservice.dto.CardInfoDTO;
import hipstercard.dev.transactionservice.dto.TransactionDTO;
import hipstercard.dev.transactionservice.exception.NoHipstocardTransactionInLastPeriod;
import hipstercard.dev.transactionservice.exception.NotEnoughBalanceBeforeTransactionException;
import hipstercard.dev.transactionservice.exception.OperationCategoryWasNotFoundException;
import hipstercard.dev.transactionservice.exception.OrganizationWasNotFoundException;
import hipstercard.dev.transactionservice.exception.TransactionWasDoneWithDefaultCardException;
import hipstercard.dev.transactionservice.repository.OperationCategoryRepository;
import hipstercard.dev.transactionservice.repository.OrganizationFlexibleCashbackRepository;
import hipstercard.dev.transactionservice.repository.OrganizationRepository;
import hipstercard.dev.transactionservice.repository.TransactionRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@Slf4j
public class TransactionServiceImpl implements TransactionService {
    private static final String DEFAULT_CARD_TYPE_NAME = "DEFAULT";
    private static final long MIN_TIME_DIFFERENCE_FOR_CASHBACK_MS = 2592000000L;

    private final CardsClient cardsClient;
    private final TransactionRepository transactionRepository;
    private final OrganizationRepository organizationRepository;
    private final OperationCategoryRepository categoryRepository;
    private final OrganizationFlexibleCashbackRepository flexibleCashbackRepository;

    @Override
    public List<TransactionDTO> formCashbackTransactions(List<TransactionDTO> transactions) {
        List<TransactionDTO> cashbackTransactions = new ArrayList<>();
        for (TransactionDTO transaction : transactions) {
            if (isValidOrganizationAndOperationCategory(transaction)) {
                log.info("Now handling transaction: {}", transaction);
                TransactionDTO cashbackTransaction = formCashbackTransaction(transaction);

                saveCashbackTransaction(cashbackTransaction);
                log.info("Cashback transaction saved successfully!");

                cashbackTransactions.add(cashbackTransaction);
            } else {
                transaction.setTransactionSum(BigDecimal.ZERO);
                cashbackTransactions.add(transaction);
            }
        }

        return cashbackTransactions;
    }

    private boolean isValidOrganizationAndOperationCategory(TransactionDTO transaction) {
        Optional<Organization> organization = organizationRepository.findByCode(transaction.getOrganizationCode());
        Optional<OperationCategory> operationCategory = categoryRepository.findByName(transaction.getOperationCategory());
        return organization.isPresent() && operationCategory.isPresent();
    }

    private TransactionDTO formCashbackTransaction(TransactionDTO transaction) {
        transaction.setTransactionSum(calculateCashback(transaction));
        log.info("Calculated cashback sum: {}", transaction.getTransactionSum());
        return transaction;
    }

    private BigDecimal calculateCashback(TransactionDTO transaction) {
        try {
            BankAccountCardsDTO bankAccountCards = cardsClient.getLinkedCards(BankAccountDTO.builder()
                    .bankAccountNumber(transaction.getBankAccountNumber())
                    .build());
            log.info("Got linked cards: {}", bankAccountCards);

            CardInfoDTO hipstercard = extractHipsterCard(transaction, bankAccountCards);
            log.info("Extracted hipstercard: {}", hipstercard);

            checkCashbackConditions(transaction, hipstercard);
            log.info("Successfully checked cashback conditions");

            return calculateCashbackSum(transaction, hipstercard);
        } catch (Exception exception) {
            log.info("Catch {} exception with message: {}", exception.getClass().getSimpleName(),
                    exception.getMessage());
            return BigDecimal.ZERO;
        }
    }

    private CardInfoDTO extractHipsterCard(TransactionDTO transaction,
                                           BankAccountCardsDTO bankAccountCards) {
        for (CardInfoDTO cardInfo : bankAccountCards.getCards()) {
            if (cardInfo.getId().equals(transaction.getCardId())) {
                return getIfHipstercardOrElseThrow(cardInfo);
            }
        }

        throw new IllegalStateException();
    }

    private CardInfoDTO getIfHipstercardOrElseThrow(CardInfoDTO cardInfo) {
        if (cardInfo.getCardType().getName().equals(DEFAULT_CARD_TYPE_NAME)) {
            throw new TransactionWasDoneWithDefaultCardException();
        }

        return cardInfo;
    }

    private void checkCashbackConditions(TransactionDTO transaction, CardInfoDTO hipstercard) {
        checkMinBalanceBeforeTransaction(transaction, hipstercard);
        log.info("Min balance check successful!");

        checkTransactionInLastPeriod(transaction, hipstercard);
        log.info("Last period check successful!");
    }

    private void checkMinBalanceBeforeTransaction(TransactionDTO transaction, CardInfoDTO hipstercard) {
        BigDecimal balanceBeforeTransaction = transaction.getBalanceAfterTransaction()
                .subtract(transaction.getTransactionSum());
        if (balanceBeforeTransaction.compareTo(hipstercard.getCardType()
                .getMinBalanceBeforeTransactionForCashback()) < 0) {
            throw new NotEnoughBalanceBeforeTransactionException();
        }
    }

    private void checkTransactionInLastPeriod(TransactionDTO transaction,
                                              CardInfoDTO hipstercard) {
        List<Transaction> transactions = transactionRepository.findAllByCardId(hipstercard.getId());
        for (Transaction savedTransaction : transactions) {
            long timeDifferenceMs = ChronoUnit.MILLIS
                    .between(transaction.getDate(), savedTransaction.getDate());

            if (timeDifferenceMs < MIN_TIME_DIFFERENCE_FOR_CASHBACK_MS) {
                return;
            }
        }

        throw new NoHipstocardTransactionInLastPeriod();
    }

    private BigDecimal calculateCashbackSum(TransactionDTO transaction, CardInfoDTO hipstercard) {
        BigDecimal maxCashbackPercent = calculateMaxCashbackPercent(transaction, hipstercard);
        log.info("Max cashback percent is: {}", maxCashbackPercent);

        return transaction.getTransactionSum().multiply(maxCashbackPercent)
                .divide(BigDecimal.valueOf(100), 2, RoundingMode.DOWN);
    }

    private BigDecimal calculateMaxCashbackPercent(TransactionDTO transaction, CardInfoDTO hipstercard) {
        BigDecimal cardCashbackPercent = hipstercard.getCardType().getCashbackPercent();
        log.info("Card cashback percent is: {}", cardCashbackPercent);

        BigDecimal organizationCashbackPercent = getOrganizationCashbackPercent(transaction.getOrganizationCode());
        log.info("Organization cashback percent is: {}", organizationCashbackPercent);

        BigDecimal categoryCashbackPercent = getCategoryCashbackPercent(transaction.getOperationCategory());
        log.info("Category cashback percent is: {}", categoryCashbackPercent);

        BigDecimal organizationFlexibleCashbackPercent =
                getOrganizationFlexibleCashbackPercent(transaction.getOrganizationCode(),
                        transaction.getTransactionSum());
        log.info("Organization flexible cashback is: {}", organizationFlexibleCashbackPercent);

        List<BigDecimal> cashbackPercents = List.of(cardCashbackPercent, organizationCashbackPercent,
                categoryCashbackPercent, organizationFlexibleCashbackPercent);

        return cashbackPercents.stream()
                .max(Comparator.naturalOrder())
                .orElse(BigDecimal.ZERO);
    }

    private BigDecimal getOrganizationCashbackPercent(String organizationCode) {
        Optional<Organization> organization = organizationRepository.findByCode(organizationCode);
        if (organization.isEmpty()) {
            throw new OrganizationWasNotFoundException();
        }

        if (organization.get().getContributionStatus().equals(ContributionStatus.APPROVED)) {
            return organization.get().getCashbackPercent();
        }

        return BigDecimal.ZERO;
    }

    private BigDecimal getCategoryCashbackPercent(String categoryName) {
        Optional<OperationCategory> category = categoryRepository.findByName(categoryName);
        if (category.isEmpty()) {
            throw new OperationCategoryWasNotFoundException();
        }

        if (category.get().getContributionStatus().equals(ContributionStatus.APPROVED)) {
            return category.get().getCashbackPercent();
        }

        return BigDecimal.ZERO;
    }

    private BigDecimal getOrganizationFlexibleCashbackPercent(String organizationCode, BigDecimal transactionSum) {
        List<OrganizationFlexibleCashback> flexibleCashbacks =
                flexibleCashbackRepository.findAllByOrganizationCode(organizationCode);

        log.info("Started checking flexible cashbacks. Got {} for this organization", flexibleCashbacks.size());

        BigDecimal flexibleOrganizationCashback = BigDecimal.ZERO;
        for (OrganizationFlexibleCashback organizationFlexibleCashback : flexibleCashbacks) {
            log.info("Now cashback is: {}%, current flexible cashback from db is: {}%", flexibleOrganizationCashback,
                    organizationFlexibleCashback.getCashbackPercent());

            if (organizationFlexibleCashback.getContributionStatus().equals(ContributionStatus.APPROVED)
                    && transactionSum.compareTo(organizationFlexibleCashback.getMinTransactionSum()) >= 0) {
                flexibleOrganizationCashback = flexibleOrganizationCashback.max(organizationFlexibleCashback.getCashbackPercent());
                log.info("It is bigger than now, so now cashback became: {}%", flexibleOrganizationCashback);
            }
        }

        return flexibleOrganizationCashback;
    }

    private void saveCashbackTransaction(TransactionDTO cashbackTransaction) {
        Transaction cashbackTransactionToSave = Transaction.builder()
                .id("transaction:" + UUID.randomUUID())
                .bankAccountNumber(cashbackTransaction.getBankAccountNumber())
                .cardId(cashbackTransaction.getCardId())
                .date(cashbackTransaction.getDate())
                .transactionSum(cashbackTransaction.getTransactionSum())
                .balanceBeforeTransaction(cashbackTransaction.getBalanceAfterTransaction()
                        .subtract(cashbackTransaction.getTransactionSum()))
                .organization(organizationRepository.findByCode(cashbackTransaction.getOrganizationCode()).get())
                .operationCategory(categoryRepository.findByName(cashbackTransaction.getOperationCategory()).get())
                .build();

        transactionRepository.save(cashbackTransactionToSave);
    }
}
