package hipstercard.dev.transactionservice.service;

import hipstercard.dev.transactionservice.dto.TransactionDTO;

import java.util.List;

public interface TransactionService {
    List<TransactionDTO> formCashbackTransactions(List<TransactionDTO> transactions);
}
