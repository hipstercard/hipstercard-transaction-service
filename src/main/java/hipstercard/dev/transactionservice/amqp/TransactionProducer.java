package hipstercard.dev.transactionservice.amqp;

import hipstercard.dev.transactionservice.dto.TransactionDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class TransactionProducer {
    private final AmqpTemplate amqpTemplate;

    public void publishTransactions(List<TransactionDTO> cashbackTransactions, String exchange, String routingKey) {
        amqpTemplate.convertAndSend(exchange, routingKey, cashbackTransactions);
    }
}
