package hipstercard.dev.transactionservice.amqp;

import hipstercard.dev.transactionservice.dto.TransactionDTO;
import hipstercard.dev.transactionservice.service.TransactionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
@RequiredArgsConstructor
public class TransactionListener {
    private final TransactionProducer transactionProducer;
    private final TransactionService transactionService;

    @Value("${spring.rabbitmq.exchanges.cashback-transaction-internal}")
    private String internalExchange;

    @Value("${spring.rabbitmq.routing-keys.internal-cashback-transaction}")
    private String internalCashbackTransactionRoutingKey;

    @RabbitListener(queues = "${spring.rabbitmq.queues.transaction}")
    public void receiveTransactions(List<TransactionDTO> transactions) {
        log.info("Received transactions: {}", transactions);
        List<TransactionDTO> cashbackTransactions = transactionService.formCashbackTransactions(transactions);
        log.info("Formed cashback transactions: {}", cashbackTransactions);
        transactionProducer.publishTransactions(cashbackTransactions, internalExchange,
                internalCashbackTransactionRoutingKey);
    }
}
