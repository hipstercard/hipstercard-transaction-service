# ✈️ Transactions Microservice

This microservice is responsible for handling bank transactions and create cashback transactions.

## 1. 🗿 Entities

- This microservice has 4 entity classes:

1. [OperationCategory](https://gitlab.com/hipstercard/hipstercard-transaction-service/-/blob/main/src/main/java/hipstercard/dev/transactionservice/domain/OperationCategory.java)
2. [Organization](https://gitlab.com/hipstercard/hipstercard-transaction-service/-/blob/main/src/main/java/hipstercard/dev/transactionservice/domain/Organization.java)
3. [OrganizationFlexibleCashback](https://gitlab.com/hipstercard/hipstercard-transaction-service/-/blob/main/src/main/java/hipstercard/dev/transactionservice/domain/OrganizationFlexibleCashback.java)
4. [Transaction](https://gitlab.com/hipstercard/hipstercard-transaction-service/-/blob/main/src/main/java/hipstercard/dev/transactionservice/domain/Transaction.java)

- Also there is [ContributionStatus](https://gitlab.com/hipstercard/hipstercard-transaction-service/-/blob/main/src/main/java/hipstercard/dev/transactionservice/domain/ContributionStatus.java) enum that is responsible for storing information about the status of last update: was it just created, approved, declined or even deleted

- Relationships between entities:
  + Every OperationCategory, Organization and OrganizationFlexibleCashback has their own ContributionStatus
  + Each Transaction has only one OperationCategory and one Organization
  + Each OrganizationFlexibleCashback has only one Organization

## 2. 🖥 Database and migrations

- PostgreSQL is used as main database in this microservice. It is deployed in a docker container on a remote server.
- To work with database in Java code I was using Hibernate ORM and Spring Data JPA.
- For migrations, I was using Liquibase library. All the migration scripts are stored in [scripts](https://gitlab.com/hipstercard/hipstercard-transaction-service/-/tree/main/src/main/resources/db/scripts) folder.
- The database structure can be shown in the following diagram:

![transactions-tables](src/main/resources/readme-images/transaction-tables.jpg)

## 3. 🐰 Communication via RabbitMQ

- This microservice interacts with the bank's processing center asynchronously thanks to RabbitMQ.
- It receives as input a List<TransactionDTO>, where each TransactionDTO can be represented in JSON in the following form:

```json
{
  "bankAccountNumber": "bank-account:6cc81406-ac5d-483a-b757-1755da3a309c",
  "cardId": "card:b6e579d0-6765-4c5e-be1f-2fe60276e1ab",
  "balanceAfterTransaction": 150000.00,
  "transactionSum": 10000.00,
  "organizationCode": "organization:qbe7eeb9-6fc4-497b-bb9b-4d7e1c199df4",
  "operationCategory": "VIDEOGAMES",
  "date": "2023-07-25T23:09:01.795+0200"
}
```

- Having received a list of such transactions, the microservice gives back the List of transactions in the same order, but the transactionSum field contains the amount of cashback to be credited on this transaction.

## 4. 🔄 Communication with cards-service via HTTP 

- This microservice communicate with the [Cards Microservice](https://gitlab.com/hipstercard/hipstercard-card-service) via HTTP-requests
- For http-communication between microservices, I was using FeignClients
- For example, this is a [Card Feign Client](https://gitlab.com/hipstercard/hipstercard-transaction-service/-/blob/main/src/main/java/hipstercard/dev/transactionservice/clients/CardsClient.java)
- So, this microservice send bank account number that was send in message to RabbitMQ in JSON:

```json
{
  "bankAccountNumber": "bank-account:6cc81406-ac5d-483a-b757-1755da3a309c"
}
```

- And receives back all cards linked to this bank account in JSON:

```json
{
  "cards": [
    {
      "id": "card:b6e579d0-6765-4c5e-be1f-2fe60276e1ab",
      "cardType": {
        "name": "HIPSTER_PLATINUM",
        "cashbackPercent": 2.00,
        "minBalanceBeforeTransactionForCashback": 70000.00
      }
    }
  ]
}
```

- You can read more about Cards Microservice here: https://gitlab.com/hipstercard/hipstercard-card-service

## 5. ✔️ Validation

- To validate data that is passed to a RabbitMQ queue I was using Hibernate Validation and its annotations
- For example, to validate bank account number field in [BankAccountDTO](https://gitlab.com/hipstercard/hipstercard-transaction-service/-/blob/main/src/main/java/hipstercard/dev/transactionservice/dto/BankAccountDTO.java), I used this annotations:

```java
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BankAccountDTO {
  @NotBlank(message = "Bank account number should not be blank!")
  @Pattern(regexp = "^bank-account:[0-9a-fA-F]{8}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{12}$",
          message = "Bank account number should be in format bank-account:{UUID}")
  private String bankAccountNumber;
}
```

- And if some data was invalid (not existing organization code or category name and so on) the cashback amount is 0.00

## 6. 🟢 Testing

- Since it doesn't make sense to test this microservice separately because it uses Cards Microservice, I tested the whole system, so the tests involving RabbitMQ and Transactions Microservice can be found in the [e2e-tests-repo](https://gitlab.com/hipstercard/hipstercard-e2e-tests)

## 7. 🦊 Gitlab CI

- In .gitlab-ci.yml file I have following jobs and stages:
1. `install_mr` job in `install` stage - it is running when merge request to main is created (GitFlow will be there soon, my bad...). This stage build the microservice and run tests in src/test folder by executing mvn install command
2. `install` job in `install` stage - it is running when merge request was merged to main. Just do mvn clean install and generate .jar artifact that will be used in next stages to build docker image.
3. `docker_work` job in `docker_work` stage - removes unused images, build new docker image using .jar file from previous stage and push this image to a Gitlab container registry
4. `deploy` job in `deploy` stage - stops and removes previous containers, creates network, and start new container from image generated at previous stage.

## 8. 🧑🏾‍💻 Deploy

- This microservice is now deployed and is listening for new messages in Rabbit's queue 😃
